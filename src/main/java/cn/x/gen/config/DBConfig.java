package cn.x.gen.config;

import lombok.Builder;
import lombok.Data;

/**
 * 数据库连接配置
 */
@Data
@Builder
public class DBConfig {

    /**
     * 地址
     */
    private String url;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;


}
