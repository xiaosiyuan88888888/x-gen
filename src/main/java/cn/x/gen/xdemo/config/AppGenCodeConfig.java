package cn.x.gen.xdemo.config;

import lombok.Builder;
import lombok.Data;

/**
 * 应用代码生成配置
 */
@Data
@Builder
public class AppGenCodeConfig {

    /**
     * 应用名
     * note：用来找模板，和拼接生成代码路径中的模块名
     */
    private String appName;

    /**
     * 作者
     */
    private String author;

    /**
     * 目标项目路径
     */
    private String targetProjectDirLocal;

    /**
     * 包含的表名
     * 例：exercise_tbl,exercise_dtl_tbl
     */
    private String include;

}
