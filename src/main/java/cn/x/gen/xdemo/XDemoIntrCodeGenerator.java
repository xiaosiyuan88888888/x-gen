package cn.x.gen.xdemo;


import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 根据数据库中的接口字段配置，和接口模版，生成代码
 */
@Slf4j
public class XDemoIntrCodeGenerator {

    public static void main(String[] args) {
        // 流水号
        String serialId = IdUtil.getSnowflakeNextIdStr();
        log.info("流水号：{}，开始生成", serialId);


        log.info("流水号：{}，生成结束", serialId);
    }

}
