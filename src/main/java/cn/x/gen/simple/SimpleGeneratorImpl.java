package cn.x.gen.simple;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.x.gen.simple.intr.FtlGenerator;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class SimpleGeneratorImpl implements FtlGenerator {


    private String rsFileDirPathKey = "rsFileDirPath";
    private String rsFileDirPathValue = "";

    private String rsFileNameSuffixKey = "RsFileName";
    private String startMarkSuffixKey = "startMark";
    private String endMarkSuffixKey = "endMark";

    // 生成文件绝对路径集合，保证文件不会重复。
    private Set<String> rsFilePathSet = new HashSet<>();

    @Override
    public Boolean generate(String jsonDirPath, String templateDirPath) {
        AtomicBoolean rs = new AtomicBoolean(false);
        Arrays.asList(FileUtil.ls(jsonDirPath))
                .stream()
                .filter(jsonFile -> StrUtil.endWith(jsonFile.getName(),".json"))
                .forEach(jsonFile -> {
                    Arrays.asList(FileUtil.ls(templateDirPath))
                            .stream()
                            .filter(templateFile -> StrUtil.endWith(templateFile.getName(),".ftl"))
                            .forEach(templateFile -> {
                                // json文件转为json字符串
                                String jsonStr = FileUtil.readString(jsonFile, StandardCharsets.UTF_8);
                                // json字符串转为JSON对象
                                JSON json = JSONUtil.parse(jsonStr);
                                // JSON 为 JSONArray，抛出异常
                                if (json instanceof JSONArray) {
                                    // 不支持json文件中，第一个层级为Array
                                    log.error("文件绝对路径：{},不支持json文件第一个层级为Array", jsonFile.getAbsolutePath());
                                    return;
                                }
                                // JSON强转为JSONObject
                                JSONObject jsonObj = (JSONObject)json;
                                // 获取生成文件所在目录绝对路径
                                rsFileDirPathValue = jsonObj.get(rsFileDirPathKey, String.class);
                                if (StrUtil.isBlankIfStr(rsFileDirPathValue)) {
                                    log.info("文件绝对路径：{},json文件第一个层级的rsFileExtName键值对不存在或为空", jsonFile.getAbsolutePath());
                                    rsFileDirPathValue = templateDirPath;
                                }
                                // 获取文件所在目录绝对路径
                                // 获取模版文件名(不含后缀)
                                String templateFileMainName = FileUtil.mainName(templateFile);
                                // 生成文件名-json键
                                String rsFileNameKey = templateFileMainName + rsFileNameSuffixKey;
                                // 生成文件名
                                String rsFileName = jsonObj.get(rsFileNameKey, String.class);
                                if (Objects.isNull(rsFileName)) {
                                    log.error("文件绝对路径：{},json文件第一个层级的{}键值对必须存在", jsonFile.getAbsolutePath(), rsFileNameKey);
                                    return;
                                }
                                // 初始化变量Map
                                Map<String, Object> dataModelMap = new HashMap<>();
                                // JSONObject转换为变量Map
                                dataModelMap = jsonObj.toBean(dataModelMap.getClass());
                                try {
                                    // 配置 Freemarker
                                    Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);
                                    cfg.setDirectoryForTemplateLoading(new File(templateDirPath));
                                    cfg.setDefaultEncoding(StandardCharsets.UTF_8.name());
                                    // 加载 FTL 模板
                                    Template template = cfg.getTemplate(templateFile.getName());
                                    // 生成输出文件
                                    String rsFilePath = rsFileDirPathValue + File.separator + rsFileName;
                                    if (rsFilePathSet.contains(rsFilePath)) {
                                        log.error("文件绝对路径：{},存在重复", rsFilePath);
                                        return;
                                    } else {
                                        rsFilePathSet.add(rsFilePath);
                                    }
                                    File outputFile = new File(rsFilePath);
                                    FileWriter writer = new FileWriter(outputFile);
                                    // 如果存在自定义代码部分，把自定义代码部分取出并放入新的代码中
                                    if (FileUtil.exist(rsFilePath)) {


                                        // todolist

                                        // import部分，合并
                                        TreeSet<String> unJdkImportSet = new TreeSet<>();

                                        unJdkImportSet.comparator();
                                        TreeSet<String> jdkImportSet = new TreeSet<>();
                                    } else {
                                        template.process(dataModelMap, writer);
                                    }
                                    rs.compareAndSet(false,true);
                                } catch (IOException | TemplateException e) {
                                    throw new RuntimeException(e);
                                }
                            });
                });
        return rs.get();
    }

}
