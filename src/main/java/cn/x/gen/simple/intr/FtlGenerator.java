package cn.x.gen.simple.intr;

public interface FtlGenerator {

    /**
     *
     * 根据变量文件和模板文件生成文件
     *
     * @param jsonDirPath json格式变量文件所在文件夹的绝对路径
     * @param templateDirPath ftl格式模板文件所在文件夹的绝对路径
     * @return 是否生成文件成功
     */
    Boolean generate(String jsonDirPath,String templateDirPath);



}
