package cn.x;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.x.gen.simple.SimpleGeneratorImpl;
import cn.x.gen.simple.intr.FtlGenerator;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Paths;

@Slf4j
public class App {
    public static void main(String[] args) {
        Boolean rs = true;

        FtlGenerator generator = new SimpleGeneratorImpl();
        if (ArrayUtil.isEmpty(args)) {
            String jarPath = App.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            if (StrUtil.startWith(jarPath, "/") && System.getProperty("os.name").toLowerCase().contains("win")) {
                jarPath = jarPath.substring(1);
            }

            // 获取 JAR 文件所在的目录
            String jarDirectory = Paths.get(jarPath).getParent().toString();
            log.info("模版文件目录：{}，变量文件目录：{}", jarDirectory, jarDirectory);
            rs = generator.generate(jarDirectory, jarDirectory);
            if (rs) {
                log.info("生成成功！");
            } else {
                log.info("生成失败或无模版/变量文件！");
            }
        } else if (args.length == 1) {
            log.info("模版文件目录：{}，变量文件目录：{}", args[0], args[0]);
            rs = generator.generate(args[0], args[0]);
            if (rs) {
                log.info("生成成功！");
            } else {
                log.info("生成失败或无模版/变量文件！");
            }
        } else if (args.length == 2) {
            log.info("模版文件目录：{}，变量文件目录：{}", args[0], args[0]);
            rs = generator.generate(args[0], args[1]);
            if (rs) {
                log.info("生成成功！");
            } else {
                log.info("生成失败或无模版/变量文件！");
            }
        } else {
            log.error("参数长度非法，长度：{}", args.length);
        }
    }
}
