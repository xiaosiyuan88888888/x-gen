<#if boVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${boVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${boVars.packageName};

import ${boVars.dataPackageName}.${entity};
import lombok.Data;
import lombok.experimental.Accessors;

/**
* <p>
* ${table.comment!}-BO对象
* </p>
*
* @author ${author}
*/
@Data
@Accessors(chain = true)
public class ${entity}BO extends ${entity} {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>


    ${abs.customerAreaStartMark}
${boVars.customerArea}
    ${abs.customerAreaEndMark}

}
