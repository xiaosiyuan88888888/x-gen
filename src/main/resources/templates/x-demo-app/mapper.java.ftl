<#if mapperVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${mapperVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${mapperVars.packageName};

import ${doVars.packageName}.${entity}DO;
import ${superMapperClassPackage};
<#if mapperAnnotationClass??>
import ${mapperAnnotationClass.name};
</#if>

/**
 * <p>
 * ${table.comment!} Mapper 接口
 * </p>
 *
 * @author ${author}
 */
<#if mapperAnnotationClass??>
@${mapperAnnotationClass.simpleName}
</#if>
public interface ${table.mapperName} extends ${superMapperClass}<${entity}DO> {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>

    ${abs.customerAreaStartMark}
 ${mapperVars.customerArea}
    ${abs.customerAreaEndMark}

}
