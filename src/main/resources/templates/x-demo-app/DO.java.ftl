<#if doVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${doVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${doVars.packageName};

import ${doVars.dataPackageName}.${entity};
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * ${table.comment!}-DO对象
 * </p>
 *
 * @author ${author}
 */
@Data
@Accessors(chain = true)
public class ${entity}DO extends ${entity} {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>


    ${abs.customerAreaStartMark}
${doVars.customerArea}
    ${abs.customerAreaEndMark}

}
