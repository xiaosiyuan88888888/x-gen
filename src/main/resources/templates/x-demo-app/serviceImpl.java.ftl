<#if serviceImplVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${serviceImplVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${serviceImplVars.packageName};

import ${doVars.packageName}.${entity}DO;
import ${package.Mapper}.${table.mapperName};
<#if generateService>
import ${package.Service}.${table.serviceName};
</#if>
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 */
@Service
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}DO><#if generateService> implements ${table.serviceName}</#if> {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>

    ${abs.customerAreaStartMark}
${serviceImplVars.customerArea}
    ${abs.customerAreaEndMark}

}
