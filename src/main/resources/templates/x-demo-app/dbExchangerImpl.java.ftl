<#if dbExchangerImplVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${dbExchangerImplVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${dbExchangerImplVars.packageName};

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.x.abs.common.enums.IsDelEnum;
import com.x.abs.common.exception.XBizException;
import com.x.abs.common.pagination.XPageDTO;
import com.x.abs.common.spring.util.XBeanUtils;
import com.x.domain.bo.CanaryUserBO;
import com.x.domain.exchanger.CanaryUserDbExchanger;
import com.x.infra.data.CanaryUser;
import com.x.infra.mapper.CanaryUserMapper;
import com.x.infra.myservice.CanaryUserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class CanaryUserDbExchangerImpl implements CanaryUserDbExchanger {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>

    @Resource
    CanaryUserMapper canaryUserMapper;

    @Resource
    CanaryUserService canaryUserService;

    @Override
    public CanaryUserBO getById(Integer id) {
        LambdaQueryWrapper<CanaryUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(Objects.nonNull(id), CanaryUser::getId, id)
                .eq(CanaryUser::getIsDel, IsDelEnum.N.getValue());
        CanaryUser canaryUser = canaryUserService.getOne(queryWrapper);
        return XBeanUtils.convert(canaryUser, CanaryUserBO.class);
    }

    @Override
    public int deleteById(Integer id) {
        // 作逻辑删除
        LambdaUpdateWrapper<CanaryUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper
                .eq(Objects.nonNull(id), CanaryUser::getId, id)
                .eq(CanaryUser::getIsDel, IsDelEnum.N.getValue())
                .set(CanaryUser::getIsDel, IsDelEnum.Y.getValue());
        canaryUserService.update(updateWrapper);
        return id;
    }

    @Override
    public int saveCanaryUser(CanaryUserBO canaryUserBO) {
        canaryUserBO.setCrtTme(new Date());
        CanaryUser canaryUser = XBeanUtils.convert(canaryUserBO, CanaryUser.class);
        canaryUserMapper.insert(canaryUser);
        return canaryUser.getId();
    }

    @Override
    public int updateCanaryUser(CanaryUserBO canaryUserBO) {
        canaryUserBO.setUpdTme(new Date());
        CanaryUser canaryUser = XBeanUtils.convert(canaryUserBO, CanaryUser.class);
        int i = canaryUserMapper.updateById(canaryUser);
        if (0 == i) {
            throw new XBizException("更新失败");
        }
        return canaryUser.getId();
    }

    @Override
    public List<CanaryUserBO> listCanaryUserByName(String usrName) {
        List<CanaryUser> canaryUserList = canaryUserService.listCanaryUserByName(usrName);
        List<CanaryUserBO> canaryUserBOList = XBeanUtils.convert(canaryUserList, CanaryUserBO.class);
        return canaryUserBOList;
    }

    @Override
    public XPageDTO<CanaryUserBO> listCanaryUserByPage(CanaryUserBO canaryUserBO, long pageSize, long pageNum) {
        CanaryUser canaryUser = XBeanUtils.convert(canaryUserBO, CanaryUser.class);
        XPageDTO<CanaryUser> canaryUserXPage = canaryUserService.listCanaryUserByPage(canaryUser, pageSize, pageNum);
        XPageDTO<CanaryUserBO> convert = XBeanUtils.convert(canaryUserXPage, CanaryUserBO.class);
        return convert;
    }

    ${abs.customerAreaStartMark}
${dbExchangerImplVars.customerArea}
    ${abs.customerAreaEndMark}


}
