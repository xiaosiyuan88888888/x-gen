<#if dtoVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${dtoVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${dtoVars.packageName};

import ${dtoVars.dataPackageName}.${entity};
import lombok.Data;
import lombok.experimental.Accessors;

/**
* <p>
* ${table.comment!}-DTO对象
* </p>
*
* @author ${author}
*/
@Data
@Accessors(chain = true)
public class ${entity}DTO extends ${entity} {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>


    ${abs.customerAreaStartMark}
${dtoVars.customerArea}
    ${abs.customerAreaEndMark}

}
