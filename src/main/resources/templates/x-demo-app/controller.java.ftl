<#if controllerVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${controllerVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${controllerVars.packageName};

import ${apiVars.packageName}.${entity}Api;
import ${dtoVars.packageName}.${entity}DTO;
import org.springframework.web.bind.annotation.RequestMapping;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.x.abs.common.rpc.XResponse;
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} implements ${entity}Api {
<#else>
public class ${table.controllerName} implements ${entity}Api {
</#if>
${abs.importAnnotationClassDefineAreaEndMark}
</#if>

    @PostMapping(value = "/${entity?substring(0,1)?lower_case + entity?substring(1)}/save")
    public XResponse<Integer> save(@RequestBody ${entity}DTO ${entity?substring(0,1)?lower_case + entity?substring(1)}DTO) {
        return null;
    }


    ${abs.customerAreaStartMark}
${controllerVars.customerArea}
    ${abs.customerAreaEndMark}

}

