<#if serviceVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${serviceVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${serviceVars.packageName};

 import ${doVars.packageName}.${entity}DO;
import ${superServiceClassPackage};

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 */
public interface ${table.serviceName} extends ${superServiceClass}<${entity}DO> {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>

    ${abs.customerAreaStartMark}
 ${serviceVars.customerArea}
    ${abs.customerAreaEndMark}

}
