<#if apiVars.importAnnotationClassDefineArea?has_content>
${abs.importAnnotationClassDefineAreaStartMark}
${apiVars.importAnnotationClassDefineArea}
${abs.importAnnotationClassDefineAreaEndMark}
<#else>
${abs.importAnnotationClassDefineAreaStartMark}
package ${apiVars.packageName};

import com.x.abs.common.rpc.XResponse;
import ${dtoVars.packageName}.${entity}DTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

import com.x.abs.common.pagination.XPageDTO;
import com.x.abs.common.pagination.XPageQueryWith;
import com.x.abs.common.rpc.XListResponse;
import org.springframework.web.bind.annotation.*;

/**
* <p>
* ${table.comment!}-接口定义
* </p>
*
* @author ${author}
*/
public interface ${entity}Api {
${abs.importAnnotationClassDefineAreaEndMark}
</#if>

    @PostMapping(value = "/${entity?substring(0,1)?lower_case + entity?substring(1)}/save")
    XResponse<Integer> save(@Valid @RequestBody ${entity}DTO canaryUser, BindingResult result);

    @DeleteMapping(value = "/${entity?substring(0,1)?lower_case + entity?substring(1)}/{id}")
    XResponse<Integer> delete(@PathVariable(value = "id") Integer id);

    @PostMapping(value = "/${entity?substring(0,1)?lower_case + entity?substring(1)}/update")
    XResponse<Integer> update(@RequestBody ${entity}DTO ${entity?substring(0,1)?lower_case + entity?substring(1)}User);

    @GetMapping(value = "/${entity?substring(0,1)?lower_case + entity?substring(1)}/{id}")
    XResponse<${entity}DTO> getById(@PathVariable(value = "id") Integer id);

    ${abs.customerAreaStartMark}
${apiVars.customerArea}
    ${abs.customerAreaEndMark}

}