package cn.x.gen;

import cn.x.gen.simple.SimpleGeneratorImpl;
import junit.framework.TestCase;


public class SimpleGeneratorImplTest extends TestCase {

    public void testGenerate() {
        SimpleGeneratorImpl simpleGenerator = new SimpleGeneratorImpl();
        String dirPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        simpleGenerator.generate(dirPath, dirPath);
    }

}