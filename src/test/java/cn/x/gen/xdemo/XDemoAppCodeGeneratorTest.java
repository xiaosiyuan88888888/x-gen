package cn.x.gen.xdemo;

import cn.hutool.core.util.IdUtil;
import cn.x.gen.config.DBConfig;
import cn.x.gen.xdemo.config.AppGenCodeConfig;
import com.xieyang.xtools.utils.XSerialNoUtil;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XDemoAppCodeGeneratorTest extends TestCase {

    /**
     * canary_user_tbl表生成
     */
    public void testGen() {
        // 流水号
        String serialId = XSerialNoUtil.setId();
        log.info("流水号：{}，开始生成", serialId);
        // 数据库连接配置
        DBConfig dbConfig = DBConfig.builder()
                // 地址
                .url("jdbc:mysql://192.168.137.72:3306/hicash-db")
                // 用户名
                .userName("root")
                // 密码
                .password("123456")
                .build();
        // 代码生成配置
        AppGenCodeConfig genCodeConfig = AppGenCodeConfig.builder()
                // 生成代码所在的项目路径
                .targetProjectDirLocal("D:/code/x-demo-app")
                // 应用名 用于匹配该应用的模板
                .appName("x-demo-app")
                // 作者
                .author("xsy")
                // 包含的表名
                .include("canary_user_tbl")
                .build();
        // 构造应用代码生成器
        AppCodeGenerator codeGenerator = new AppCodeGenerator(dbConfig, genCodeConfig);
        // 调用生成方法
        codeGenerator.gen();
        // 校验生成结果
        log.info("流水号：{}，生成结束", serialId);
    }

}