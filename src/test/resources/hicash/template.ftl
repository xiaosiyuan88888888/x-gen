<#--引入外部模版-->
<#import "utils/StrUtils.ftl" as StrUtils>
<#---->
<#--定义变量-->
<#assign lowerCode = StrUtils.lowerFirst(code)>
<#---->
<#macro touchFile>
创建文件命令：
touch D:/code/caas-activecore-custom-app/caas-activecore-custom-app-dto/src/main/java/com/haiercash/caasactivecore/custom/dto/${code}RequestDTO.java
touch D:/code/caas-activecore-custom-app/caas-activecore-custom-app-dto/src/main/java/com/haiercash/caasactivecore/custom/dto/${code}ResponseDTO.java
touch D:/code/caas-activecore-custom-app/caas-activecore-custom-app-domain/src/main/java/com/haiercash/caasactivecore/custom/domain/bo/${code}RequestBO.java
touch D:/code/caas-activecore-custom-app/caas-activecore-custom-app-domain/src/main/java/com/haiercash/caasactivecore/custom/domain/bo/${code}ResponseBO.java
touch D:/code/caas-activecore-custom-app/caas-activecore-custom-app-domain/src/main/java/com/haiercash/caasactivecore/custom/domain/service/${code}Service.java

</#macro>
<@touchFile></@touchFile>
<#macro api>
-------------------------------------------------------Api.java-------------------------------------------------------

    @ApiOperation(value = "${name}")
    @PostMapping(value = "/${code}")
    HiResponse<${code}ResponseDTO> trade${code}(@RequestBody HiRequest<${code}RequestDTO> request);



-------------------------------------------------------Api.java-------------------------------------------------------

</#macro>
<@api></@api>
<#macro controller>
-------------------------------------------------------Controller.java-start-------------------------------------------------------


    @Resource
    Trade${code}Service trade${code}Service;

    @Override
    public HiResponse<${code}ResponseDTO> trade${code}(@RequestBody HiRequest<${code}ResponseDTO> request) {
        ${code}ResponseBO ${lowerCode}RequestBO = HiBeanUtils.convert(request.getData(), ${code}RequestBO.class);
        String serNo = "";
        if (request.getMeta().getExtraInfo() != null) {
            serNo = request.getMeta().readExtraInfo(CommonContants.EXTRAINFO_SERNO);
        }
        HiResponse<${code}ResponseDTO> res = HiResponse.success(
                HiBeanUtils.convert(
                        trade${code}Service.trade(
                            ${lowerCode}RequestBO
                        ), ${code}ResponseDTO.class
                )
        ).ofExtraInfo(CommonContants.EXTRAINFO_RETFLAG, ParamIntrReturnEnum.SUCCESS.getValue())
            .ofExtraInfo(CommonContants.EXTRAINFO_RETMSG, ParamIntrReturnEnum.SUCCESS.getDesc());
        if (StringUtils.isNotBlank(serNo)) {
            res.ofExtraInfo(CommonContants.EXTRAINFO_SERNO, serNo);
        }
        return res;
    }


-------------------------------------------------------Controller.java-end-------------------------------------------------------
</#macro>
<@controller></@controller>
<#macro requestDTO>
-------------------------------------------------------RequestDTO.java-start-------------------------------------------------------
package ${dtoPackageName};

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
* ${code}-${name}-请求
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "主动核算-${name}-请求")
public class ${code}RequestDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 合同号
    */
    @ApiModelProperty(value = "合同号", required = true)
    private String contNo;

    /**
    * 客户号
    */
    @ApiModelProperty(value = "客户号", required = false)
    private String custNo;

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }

}

-------------------------------------------------------RequestDTO.java-end-------------------------------------------------------
</#macro>
<@requestDTO></@requestDTO>
<#macro responseDTO>
-------------------------------------------------------ResponseDTO.java-start-------------------------------------------------------
package ${dtoPackageName};

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
* ${code}-${name}-返回
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "主动核算-${name}-返回")
public class ${code}ResponseDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }

}

-------------------------------------------------------ResponseDTO.java-end-------------------------------------------------------
</#macro>
<@responseDTO></@responseDTO>
<#macro requestBO>
-------------------------------------------------------RequestBO.java-start-------------------------------------------------------
package ${boPackageName};

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;


/**
* ${code}-${name}-请求
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ${code}RequestBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 合同号
    */
    @NotBlank(message = "合同号不能为空")
    @Length(max = 50, message = "合同号长度不能超过50")
    private String contNo;

    /**
    * 客户号
    */
    private String custNo;


    @Override
    public String toString() {
    return JSONObject.toJSONString(this);
    }

}
-------------------------------------------------------RequestBO.java-end-------------------------------------------------------
</#macro>
<@requestBO></@requestBO>


<#macro responseBO>
-------------------------------------------------------ResponseBO.java-start-------------------------------------------------------
package ${boPackageName};

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
* ${code}-${name}-返回
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ${code}ResponseBO implements Serializable {

    private static final long serialVersionUID = 1L;


    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }

}
-------------------------------------------------------ResponseBO.java-end-------------------------------------------------------
</#macro>
<@responseBO></@responseBO>
<#macro service>
-------------------------------------------------------Service.java-start-------------------------------------------------------
package ${servicePackageName};


import com.haiercash.boot.common.code.HiStatusCode;
import com.haiercash.boot.common.code.HiStatusConstant;
import com.haiercash.boot.common.exception.HiBizException;
import com.haiercash.caasactivecore.custom.common.constant.CommonContants;
import com.haiercash.caasactivecore.custom.common.enums.*;
import com.haiercash.caasactivecore.custom.domain.bo.*;
import com.haiercash.caasactivecore.custom.domain.exchanger.ApCustNoMpngDbExchanger;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
* ${code}-${name}-服务
*/
@Service
@Slf4j
public class Trade${code}Service {

    @Resource
    private ApCustNoMpngDbExchanger apCustNoMpngDbExchanger;
    @Resource
    private ValidateService validateService;

    /**
    * ${code}-${name}-主方法
    */
    public ${code}ResponseBO trade(${code}RequestBO req) {
        try {
            ${code}ResponseBO res = new ${code}ResponseBO();
            // 入参校验
            validateService.validateResult(req, IntrReturnEnum.EI00000099.getValue());
            // 获取客户号
            if (StringUtils.isEmpty(req.getCustNo()) && StringUtils.isNotEmpty(req.getContNo())) {
                String custNo = apCustNoMpngDbExchanger.queryCustNoByContNo(req.getContNo());
                if (StringUtils.isBlank(custNo)) {
                    returnError(HiStatusConstant.E40400, IntrReturnEnum.EI00400001.getValue(), IntrReturnEnum.EI00400001.getDesc());
                } else {
                    req.setCustNo(custNo);
                }
            }
            return res;
        } catch (HiBizException e) {
            throw e;
        } catch (Exception e) {
            // 返回标志：EI00000100，返回信息：交易失败
            throw HiBizException.create(HiStatusConstant.E40400, IntrReturnEnum.EI00000100.getDesc(), e)
                .ofExtraInfo(CommonContants.EXTRAINFO_RETFLAG, IntrReturnEnum.EI00000100.getValue())
                .ofExtraInfo(CommonContants.EXTRAINFO_RETMSG, IntrReturnEnum.EI00000100.getDesc());
        }
    }

    /**
    * 返回错误信息
    */
    private void returnError(HiStatusCode hiStatusCode, String retFlag, String retMsg) {
        throw HiBizException.create(hiStatusCode, retMsg).ofExtraInfo(CommonContants.EXTRAINFO_RETFLAG, retFlag).ofExtraInfo(CommonContants.EXTRAINFO_RETMSG, retMsg);
    }

}
-------------------------------------------------------Service.java-end-------------------------------------------------------
</#macro>
<#-- 调用宏并将其结果赋值给变量 result -->
<#assign serviceResult><@service></@service></#assign>
<#-- 输出变量的内容 -->
${serviceResult}