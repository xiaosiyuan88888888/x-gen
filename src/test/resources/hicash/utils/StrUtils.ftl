
<#-- ------------------------------定义内部plain变量-start----------------------- -->
<#assign mail = "jsmith@acme.com">
<#-- ------------------------------定义内部plain变量-end----------------------- -->


<#-- ------------------------------定义宏-start----------------------- -->

<#-- ------------------------------定义宏-end----------------------- -->

<#-- ------------------------------定义函数-start----------------------- -->

<#function lowerFirst str>
    <#if str?has_content>
    <#-- 如果 str 不为空，则将首字母小写，其余部分保持不变 -->
        <#local rs = str?substring(0, 1)?lower_case + str?substring(1)>
        <#return rs>
    <#else>
    <#-- 如果 str 为空，则返回空字符串 -->
        <#return "">
    </#if>
</#function>
<#-- ------------------------------定义函数-end----------------------- -->






